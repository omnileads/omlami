FROM python:3.10-slim-bullseye  as run

WORKDIR /app
COPY ami.py requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "ami.py" ]
