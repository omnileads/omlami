import time
import redis
import pystrix
import os
import json
import logging
import sys

ASTERISK_HOSTNAME = os.getenv('ASTERISK_HOSTNAME', 'acd')
AMI_USERNAME = os.getenv('AMI_USER', 'omnileadsami')
AMI_PASSWORD = os.getenv('AMI_PASSWORD', '5_MeO_DMT')

REDIS_HOSTNAME = os.getenv('REDIS_HOSTNAME', 'redis')
REDIS_PORT = int(os.getenv('REDIS_PORT', '6379'))
REDIS_DB = int(os.getenv('REDIS_DB', '2'))

CALLDATA_QUEUE_SIZE_KEY = 'OML:CALLDATA:QUEUE-SIZE:{0}'
CALLDATA_QUEUE_KEY = 'OML:CALLDATA:QUEUE:{0}'
CALLEVENTS_CHANNEL = 'OML:CHANNEL:CALLEVENTS'


root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
stdout_handler.setFormatter(formatter)
root_logger.addHandler(stdout_handler)


class AMICore(object):

    _manager = None
    _kill_flag = False

    def __init__(self):
        self._manager = pystrix.ami.Manager()
        self._redis = redis.StrictRedis(host=REDIS_HOSTNAME, port=REDIS_PORT, db=REDIS_DB)

        self._register_callbacks()

        try:
            # Attempt to connect to Asterisk
            self._manager.connect(ASTERISK_HOSTNAME)

            challenge_response = self._manager.send_action(pystrix.ami.core.Challenge())

            if challenge_response and challenge_response.success:
                action = pystrix.ami.core.Login(
                 AMI_USERNAME, AMI_PASSWORD, challenge=challenge_response.result['Challenge']
                )
                self._manager.send_action(action)

            else:
                self._kill_flag = True
                raise ConnectionError(
                 "Asterisk did not provide an MD5 challenge token" +
                 (challenge_response is None and ': timed out' or '')
                )
        except pystrix.ami.ManagerSocketError as e:
            self._kill_flag = True
            raise ConnectionError("Unable to connect to Asterisk server: %(error)s" % {
             'error': str(e),
            })
        except pystrix.ami.core.ManagerAuthError as reason:
            self._kill_flag = True
            raise ConnectionError("Unable to authenticate to Asterisk server: %(reason)s" % {
             'reason': reason,
            })
        except pystrix.ami.ManagerError as reason:
            self._kill_flag = True
            raise ConnectionError("An unexpected Asterisk error occurred: %(reason)s" % {
             'reason': reason,
            })

        self._manager.monitor_connection()
        # self._send_queue_status()

    # def _send_queue_status(self):
    #     while not self._kill_flag:
    #         action = pystrix.ami.core.QueueStatus(queue="2_test_entrante_01")
    #         self._manager.send_action(action)
    #         time.sleep(5)

    def _register_callbacks(self):
        # Queue events
        self._manager.register_callback('QueueCallerJoin', self._handle_string_event)
        self._manager.register_callback('QueueCallerLeave', self._handle_string_event)
        # self._manager.register_callback('QueueCallerAbandon', self._handle_string_event)
        # self._manager.register_callback('QueueParams', self._handle_string_event)

        # self._manager.register_callback(None, self._handle_event)

        self._manager.register_callback('Shutdown', self._handle_shutdown)

    def _handle_shutdown(self, event, manager):
        self._kill_flag = True

    def _handle_event(self, event, manager):
        print("Received event: %s" % event.name)

    # Queue events handlers
    def _handle_string_event(self, event, manager):
        # root_logger.info("Received string event: %s" % event.name)
        # root_logger.info("Event Data: %s" % event)

        if event.name in ['QueueCallerJoin', 'QueueCallerLeave']:
            calls = event.get('Count', None)
            queue = event.get('Queue', None)
            callid = event.get('Uniqueid', None)
            campaign_id = queue[0:queue.find('_')]
            timestamp = time.time()
            delta = '1'
            if event.name != 'QueueCallerJoin':
                delta = '-1'
            if calls is not None:
                redis_key = CALLDATA_QUEUE_SIZE_KEY.format(campaign_id)
                self._redis.set(redis_key, calls)

                # Se mantiene Información de cada llamada en cada Queue
                try:
                    redis_key = CALLDATA_QUEUE_KEY.format(campaign_id)
                    if delta == '1':
                        self._redis.hset(redis_key, callid, timestamp)
                    if delta == '-1':
                        self._redis.hdel(redis_key, callid)
                except Exception as e:
                    root_logger.error(e)

                event_data = {'type': 'QUEUE', 'id': campaign_id,
                              'size': calls, 'delta': delta, 'callid': callid,
                              'timestamp': timestamp}
                self._redis.publish(CALLEVENTS_CHANNEL, json.dumps(event_data))
            else:
                pass
                # print("Position in queue: Not available")

        # if event.name == "QueueParams":
        #     #calls = event.get('Count', None)
        #     queue = event.get('Queue', None)

        #     #campaign_id = queue[0]
        #     if queue is not None:
        #         print("queue name:", queue)
        #     #    redis_key = f'OML:CAMP:{campaign_id}'
        #     #    self._redis.hset(redis_key, 'ON_QUEUE', calls)
        #     #else:
        #     #    print("Position in queue: Not available")

    def _handle_class_event(self, event, manager):
        print("Received class event: %s" % event.name)

    def is_alive(self):
        return not self._kill_flag

    def kill(self):
        self._manager.close()


class Error(Exception):
    """
    The base class from which all exceptions native to this module inherit.
    """


class ConnectionError(Error):
    """
    Indicates that a problem occurred while connecting to the Asterisk server
    or that the connection was severed unexpectedly.
    """


if __name__ == '__main__':
    ami_core = AMICore()

    while ami_core.is_alive():
        time.sleep(5)
    ami_core.kill()
